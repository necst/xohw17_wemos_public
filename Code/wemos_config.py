import time
import struct
from pynq import MMIO
from pynq.iop import request_iop
from pynq.iop import iop_const
from pynq.iop import ARDUINO
from pynq.iop import PMODA
from pynq.iop.pmod_tmp2 import Pmod_TMP2

import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt

class Wemos_CO(object):
    def __init__(self, if_id, force=False):
        if force:
            from pynq import Overlay
            Overlay("base.bit").download()

        self.iop = request_iop(if_id, "wemos_config.bin")
        print ("bin opened")
        self.mmio = self.iop.mmio
        self.iop.start()

        # # Enable all the analog pins
        # self.mmio.write(iop_const.MAILBOX_OFFSET, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x04, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x08, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x0C, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x10, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x14, 0)

    def medium_value(data):
        dat = data-np.mean(data)

        return dat

    def notch(fc_ny_ECG):
        cutoff_hz=50.0

        N, Wn = signal.cheb2ord([cutoff_hz/fc_ny_ECG*0.7,cutoff_hz/fc_ny_ECG*1.3], [cutoff_hz/fc_ny_ECG*0.9,cutoff_hz/fc_ny_ECG*1.1], 3, 60)
        b_ECG, a_ECG = signal.cheby2(N, 60, Wn, 'stop')
        w_ECG, h_ECG = signal.freqz(b_ECG, a_ECG)   #risp in freq del filtro
        
        return b_ECG,a_ECG

    def ff(b, a, E_data):
        filtered_E = signal.filtfilt(b, a, E_data)

        return filtered_E

    def ampd(sigInput):

        sigInput = sigInput.reshape(len(sigInput), 1)

        # Create preprocessing linear fit   
        sigTime = np.arange(0, len(sigInput))

        fitPoly = np.polyfit(sigTime, sigInput, 1)
        sigFit = np.polyval(fitPoly, sigTime)

        # Detrend
        dtrSignal = sigInput - sigFit

        N = len(dtrSignal)
        L = int(np.ceil(N / 2.0)) - 1
        
        # Generate random matrix
        LSM = np.random.uniform(1.0, 2.0, size = (L,N)) # uniform + alpha = 1
        print('ampd')

        # Local minima extraction
        for k in np.arange(1, L):
            for i in range(k+1, N-k):
                if((sigInput[i-1, 0] > sigInput[i-k-1, 0]) 
                & (sigInput[i-1, 0] > sigInput[i+k-1, 0])):
                    LSM[k-1, i-1] = 0

        # Find minima
        G = np.sum(LSM, 1)
        l = np.where(G == G.min())[0]
        LSM = LSM[0:l, :]


        S = np.std(LSM, 0)

        pks = np.flatnonzero(S == 0)

        return pks   

    # Fast AMPD
    def ampdFast(sigInput, order):

        # Check if order is valid (perfectly separable)
        if(len(sigInput)%order != 0):
            print("AMPD: Invalid order, decreasing order")
            while(len(sigInput)%order != 0):
                order -= 1
            print("AMPD: Using order " + str(order))

        N = int(len(sigInput) / order / 2)

        # Loop function calls
        for i in range(0, len(sigInput)-N, N):
            print("\t sector: " + str(i) + "|" + str((i+2*N-1)))
            pksTemp = Wemos_CO.ampd(sigInput[i:(i+2*N-1)])
            if(i == 0):
                pks = pksTemp
            else:
                pks = np.concatenate((pks, pksTemp+i))
            
        # Keep only unique values
        pks = np.unique(pks)
        
        return pks

    def check(self):
        ECG_data = []

        self.mmio.write(iop_const.MAILBOX_OFFSET+0x0C, 1)

        while self.mmio.read(iop_const.MAILBOX_OFFSET + 0x10) != 1:
            if self.mmio.read(iop_const.MAILBOX_OFFSET + 0x0C) == 0:
                ECG_data.append(self.mmio.read(iop_const.MAILBOX_OFFSET))

                self.mmio.write(iop_const.MAILBOX_OFFSET + 0x0C, 1)

        ECG_data = Wemos_CO.medium_value(ECG_data)  # toglie la media  dai dati
        ADC = 2**12 #ADC a 12 bit, ci servirà per stampare voltaggio in mV nei grafici

        #EGC
        fc_ECG = 500 #fcampionamento ECG
        n_ECG = np.arange(len(ECG_data))
        t_ECG = n_ECG / fc_ECG

        #CREAZIONE FILTRO NOTCH + #STAMPA  GRAFICO della risposta in frequanza del filtro notch colore=verde
        b_ECG,a_ECG = Wemos_CO.notch(fc_ECG/2.0)

        #APPLICAZIONE FILTRO NOTCH A ECG
        filtered_ECG = Wemos_CO.ff(b_ECG, a_ECG, ECG_data)

        #GRAFICO FILTRATO ECG
        plt.title('ECG: ')
        plt.plot(t_ECG, filtered_ECG, 'r')
        plt.grid(True)
        plt.show()
        #pks = Wemos_CO.ampdFast(filtered_ECG,1)
        #soglia=np.mean(filtered_ECG[pks]/2)
        #soglia=2500
        #elf.mmio.write(iop_const.MAILBOX_OFFSET + 0x14, soglia)
        #print("soglia = ", soglia)
        #dobbiamo passare soglia al wemos mailbox al posto di 2500 in funzione position