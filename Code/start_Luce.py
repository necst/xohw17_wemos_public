from pynq.iop.wemos_luce import Wemos_LU
from pynq.iop.wemos_config import Wemos_CO
import warnings

warnings.filterwarnings('ignore')

ID = input("Insert ID number+L: ")

we_co = Wemos_CO(3, True)
we_co.check()

choose = input("Rispondi 'si' se grafico ECG è corretto: ")

if choose=="si":
    print("Configurazione riuscita!")
    we_lu = Wemos_LU(3, True)
    we_lu.read(ID)
else:
    print("Configurazione non riuscita, RIPROVA!")