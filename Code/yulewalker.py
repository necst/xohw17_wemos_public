from scipy import signal, linalg
import numpy as np
import matplotlib.pyplot as plt

class YW(object):
    """A class to fit AR model using Yule-Walker method"""

    def __init__(self, X):
        self.X = X #- np.mean(X)


    def autocorr(self, lag=10):
        c = np.correlate(self.X, self.X, 'full')
        mid = np.floor(len(c)/2).astype(int)
        acov = c[mid:mid+lag]
        acor = acov/acov[0]
        return(acor)


    def fit(self, p):
        ac = self.autocorr(p+1)
        R = linalg.toeplitz(ac[:p])
        r = ac[1:p+1]
        self.phi = -linalg.inv(R).dot(r)

        ############### VARIANCE #################
        rr = self.X
        #observ_ev = EKGR[:] - EKGR[0]
        #uk = observ_ev[p+1:]
        #rr = np.diff(observ_ev, 1, axis=0)
        wn = np.array(rr[p:])
        phi = self.phi[::-1]
        var = np.array([])
        for k in range(0,len(wn)):
            var_k = wn[k]+ np.sum(phi*rr[k:k+p])
            var = np.append(var,var_k)

        var_def = np.std(var)

        self.var = var_def


    def spectrum(self):
        a = np.concatenate([np.ones(1), self.phi])
        w, h = signal.freqz(1, a)
        #h_db = 10*np.log10(2*(np.abs(h)/len(h)))
        tot = 2*(np.abs(h)/len(h))
        f = w/np.pi



        #plt.plot(f, tot)
        plt.xlabel(r'Normalized Frequency ($\times \pi$rad/sample)')
        plt.ylabel(r'Power/frequency')
        plt.title(r'Yule-Walker Spectral Density Estimate')
        #plt.grid(True)
        #plt.show()

        #evaluate area
        startLF = 0
        endLF = 0 # =startHF
        endHF = 0
        #print("sizes: " ,f.shape,y.shape)
        for i in range(0,f.size):
            if f[i]>=0.05 and startLF==0:
                startLF = i
            if f[i]>=0.15 and endLF==0:
                endLF = i
            if f[i]>=0.4 and endHF==0:
                endHF = i
                break

        LFx = f[startLF:endLF]
        LFy = tot[startLF:endLF]
        HFx = f[endLF:endHF]
        HFy = tot[endLF:endHF]

        #print("sizes: " ,LFx.shape,LFy.shape)
        LF = np.trapz(LFy,LFx)
        HF = np.trapz(HFy,HFx)

        #print(LF,HF)

        return LF,HF