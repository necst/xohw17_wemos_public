import time
import struct
from pynq import MMIO
from pynq.iop import request_iop
from pynq.iop import iop_const
from pynq.iop import ARDUINO
from pynq.iop import PMODA
from pynq.iop.pmod_tmp2 import Pmod_TMP2
import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
import pickle
from pynq.wemos.yulewalker import YW 

class Wemos_LU(object):
    def __init__(self, if_id, force=False):
        if force:
            from pynq import Overlay
            Overlay("base.bit").download()

        self.iop = request_iop(if_id, "wemos_mailbox.bin")
        print ("bin opened")
        self.mmio = self.iop.mmio
        self.iop.start()


        # # Enable all the analog pins
        # self.mmio.write(iop_const.MAILBOX_OFFSET, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x04, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x08, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x0C, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x10, 0)
        # self.mmio.write(iop_const.MAILBOX_OFFSET + 0x14, 0)

    def medium_value(data):
        dat = data-np.mean(data)

        return dat

    def notch(fc_ny_ECG):
        cutoff_hz=50.0

        N, Wn = signal.cheb2ord([cutoff_hz/fc_ny_ECG*0.7,cutoff_hz/fc_ny_ECG*1.3], [cutoff_hz/fc_ny_ECG*0.9,cutoff_hz/fc_ny_ECG*1.1], 3, 60)
        b_ECG, a_ECG = signal.cheby2(N, 60, Wn, 'stop')
        w_ECG, h_ECG = signal.freqz(b_ECG, a_ECG)   #risp in freq del filtro

        # plt.figure(1)
        # plt.title('Chebyshev II bandstop filter fit to constraints, frequency response')
        # plt.plot((w_ECG / np.pi)*fc_ny_ECG, (np.absolute(h_ECG)), 'g')
        # plt.ylabel('Magnitude')
        # plt.xlabel('Frequency (Hz)')
        # plt.grid(which='both', axis='both')
        # plt.show()

        return b_ECG,a_ECG

    def ff(b, a, E_data):
        filtered_E = signal.filtfilt(b, a, E_data)

        return filtered_E

    def low_pass(fc_GSR,fc_ny_ECG):
        Npb = 101 #lunghezza del filtro
        lw_p = signal.firwin( Npb, fc_GSR/fc_ny_ECG)
        w_GSR, h_GSR = signal.freqz(lw_p, worN=8000)    #risp in freq del filtro
        
        # plt.figure(2)
        # plt.title('Digital filter frequency response(LOW-PASS filter): ')
        # plt.plot((w_GSR/np.pi)*fc_ny_ECG, np.absolute(h_GSR), 'g', linewidth=2)
        # plt.ylabel('Magnitude')
        # plt.xlabel('Frequency (Hz)')
        # plt.ylim(-0.05,1.05)
        # plt.xlim(0,30)
        # plt.grid(True)
        # plt.show()

        return lw_p

    def butter(f1,f2,fc_ny_ECG):
        b_butter,a_butter = signal.butter(3, [f1/fc_ny_ECG, f2/fc_ny_ECG], btype='bandpass')
        w_butter,h_butter = signal.freqz(b_butter,a_butter) #w=freq normalizzata in rad/sample , h=risp in freq

        # plt.figure(6)
        # plt.title('BUTTERWOTH frequency response')
        # plt.plot((w_butter/np.pi)*fc_ny_ECG, np.absolute(h_butter), 'g', linewidth=2) #absolute=valore assoluto
        # plt.ylim(-0.05,1.05)
        # plt.xlim(0,50)
        # plt.ylabel('Magnitude')
        # plt.xlabel('Frequency (Hz)')
        # plt.grid(True)
        # plt.show()

        return b_butter,a_butter

    def squaring(CONV_pantom):
        sq = [0] * len(CONV_pantom)
        for i in range(len(CONV_pantom)):
            sq[i] = CONV_pantom[i]**2

        squar=np.asarray(sq)    #trasformo in array

        return squar

    def moving_avarage(squar):
        N=25
        mov_av=[0]*len(squar)

        for n in range(N-1,len(squar)):
            for i in range(N-1):
                mov_av[n]=mov_av[n]+squar[n-i]
            mov_av[n]=mov_av[n]/N

        mov_avarage=np.asarray(mov_av)

        return mov_avarage

    def position(ECG_data, mov_avarage, t_ECG, soglia):
        fines = 0
        ok = 1
        k = 0
        pos = []
        peak = [0] * len(ECG_data)
        tpeak = [0] * len(ECG_data)
        inizios = 0
        while(ok):
            for i in range(fines+1,len(mov_avarage)):
                if(i==len(mov_avarage)-200):
                    ok = 0
                if(mov_avarage[i]>=soglia): #soglia 
                    inizios = i
                    break
            for i in range(inizios+1,len(mov_avarage)):
                if(mov_avarage[i]<soglia): #soglia
                    fines = i
                    break
            if inizios < fines:
                #print("inizios,fines:", inizios,fines)
                pos.append(inizios + np.argmax(mov_avarage[inizios:fines])) #posizione picco x-y
                peak[k] = mov_avarage[inizios + np.argmax(mov_avarage[inizios:fines])] #valore y picco
                tpeak[k] = t_ECG[inizios + np.argmax(mov_avarage[inizios:fines])] #valore x picco
                k = k + 1

        for i in range(0, len(ECG_data)):
            if peak[i] == 0:
                k = i
                # print (k)
                break

        tpeak_fin = np.asarray(tpeak[:k])   #trasformo lista in array
        peak_fin = np.asarray(peak[:k])

        return tpeak_fin

    def pan_tompkins(f1, f2, fc_ny_ECG, ECG_data, t_ECG, soglia): 
        #CREAZIONE FILTRO passa banda con frequenza di taglio 5-15 Hz, è un filtro Butterworth di tipo IIR + #STAMPA FILTRO passa banda
        b_butter,a_butter = Wemos_MB.butter(f1, f2, fc_ny_ECG)
        butter_ECG = Wemos_MB.ff(b_butter, a_butter, ECG_data)  #APPLICAZIONE FILTRO butterwoth a ECG

        #Filtro derivatore:prendo i coefficienti dall'algoritmo di Pan-Tompkins
        coeff_pantom = [-1/8,-2/8,0,2/8,1/8]
        CONV_pantom = np.convolve([-1/8,-2/8,0,2/8,1/8], butter_ECG, 'same')
 
        #Elevo al quadrato (raddrizzamento):
        squar = Wemos_MB.squaring(CONV_pantom)

        #filtro a media mobile:ignorato i primi N-1 campioni perchè costituiscono l'inizializzazione del filtro-->no info utili
        mov_avarage = Wemos_MB.moving_avarage(squar)

        #Posizione dei picchi R: partendo dall'inizio del segnale tramite il for identifichiamo il
        #primo campione sopra la soglia (inizios) e poi il primo campione che torna sotto la soglia(fines).
        #In questo intervallo calcoliamo il massimo valore assunto dal segnale e questo corrisponde al
        #picco R. Ripetiamo l'operazione più volte spostandoci sul segnale e identifiando tutti i massimi
        #e salvandoli nell'array m
        t_peak = Wemos_MB.position(ECG_data, mov_avarage, t_ECG, soglia)

        return t_peak

    def window(conv_GSR,t_GSR):
        GSR_mean = np.mean(conv_GSR, out=None)
        k=0
        l=0
        GSR_mean_NEUTRAL=np.array([])
        GSR_mean_EMOTION=np.array([])
        for i in range(0,2):
            GSR_mean_NEUTRAL = np.append(GSR_mean_NEUTRAL, np.mean(conv_GSR[l:15000+l], out=None))
            GSR_mean_EMOTION = np.append(GSR_mean_EMOTION, np.mean(conv_GSR[15000+l:60000+l], out=None))
            
            plt.title('Window GSR - Neutral')
            plt.plot(t_GSR[l:15000+l], conv_GSR[l:15000+l], 'r') 
            plt.grid(True)
            plt.xlabel('Time [s]')
            plt.ylabel('Skin conductance [\u03bcS]')
            plt.axis([min(t_GSR[l:15000+l]), max(t_GSR[l:15000+l]),-5,5])
            plt.show()
            plt.title('Window GSR - Emotive')
            plt.plot(t_GSR[15000+l:60000+l], conv_GSR[15000+l:60000+l], 'r')
            plt.xlabel('Time [s]')
            plt.ylabel('Skin conductance [\u03bcS]') 
            plt.axis([min(t_GSR[15000+l:60000+l]), max(t_GSR[15000+l:60000+l]),-5,5])
            plt.grid(True)
            plt.show()

            l=60000+l

        print('GSR mean Neutral: ', GSR_mean_NEUTRAL)
        print('GSR mean Emotive: ', GSR_mean_EMOTION)

        return GSR_mean_NEUTRAL, GSR_mean_EMOTION

    def tmp(tmp_data):
        deltaTMP = tmp_data[0] - tmp_data[1]

        return deltaTMP


    def HRV(EKGR):

        EKGR = np.array(EKGR)

        P = 9
        observ_ev = EKGR[:] - EKGR[0]

        i1 = np.where(observ_ev > 30)[0][0]
        i2 = np.where(observ_ev > 120)[0][0]
        i3 = np.where(observ_ev > 150)[0][0]

        rr = np.diff(observ_ev, 1, axis=0)

        #1st Window Emotion
        rr_i = rr[i1:i2]
        rr_i = rr_i-np.mean(rr_i)
        ar1 = YW(rr_i)
        ar1.fit(P)
        LF1,HF1 = ar1.spectrum()
        print("1st Window: LF = ", LF1, " HF = ",HF1)
        w1 = "\n1st Window: LF = " +str(LF1)+ " HF = "+str(HF1)+" LF/HF = "+str(LF1/HF1)

        #2nd Window Emotion
        rr_i = rr[i3:]
        rr_i = rr_i-np.mean(rr_i)
        ar1 = YW(rr_i)
        ar1.fit(P)
        LF2,HF2 = ar1.spectrum()
        print("2nd Window: LF = ", LF2, " HF = ",HF2)
        w2 = "\n2nd Window: LF = " +str(LF2)+ " HF = "+str(HF2)+ " LF/HF = "+str(LF2/HF2)

        return w1+w2


    def read(self, ID):
        ECG_data = []
        GSR_data = []
        tmp_data = []
        pmod_tmp2 = Pmod_TMP2(PMODA)
        soglia = int(input('Inserisci il valore della soglia: '))
        print("Iniziamo test!")

        i = 0
        self.mmio.write(iop_const.MAILBOX_OFFSET+0x0C, 1)

        while self.mmio.read(iop_const.MAILBOX_OFFSET + 0x10) != 1:
            if self.mmio.read(iop_const.MAILBOX_OFFSET + 0x0C) == 0:
                ECG_data.append(self.mmio.read(iop_const.MAILBOX_OFFSET))
                #EMG_data.append(self.mmio.read(iop_const.MAILBOX_OFFSET + 0x04))
                GSR_data.append(self.mmio.read(iop_const.MAILBOX_OFFSET + 0x04))  
                #print('Prelevamento ' + str(i))
                #print('ECG: ' + str(ECG_data))
                #print('EMG: ' + str(EMG_data))
                #print('GSR: ' + str(GSR_data))
                self.mmio.write(iop_const.MAILBOX_OFFSET + 0x0C, 1)
                if i%119999==0 :   #ogni 120000 dati mi prendi 1 di temperatura
                    tmp_data.append(pmod_tmp2.read())
                i = i + 1

        #print("ECG Data: "+ str(ECG_data))
        #print("GSR Data: "+ str(GSR_data))
        #print("tmp: " + str(tmp_data))
        ECG_data = Wemos_MB.medium_value(ECG_data)  # toglie la media  dai dati
        GSR_data = Wemos_MB.medium_value(GSR_data)
        ADC = 2**12 #ADC a 12 bit, ci servirà per stampare voltaggio in mV nei grafici

        #EGC/GSR
        fc_ECG = 500 #fcampionamento ECG
        fc_GSR = 5 #fcampionamento ECG
        n_ECG = np.arange(len(ECG_data))
        n_GSR = np.arange(len(GSR_data))
        t_ECG = n_ECG / fc_ECG
        #t_GSR = n_GSR/fc_GSR
        t_GSR = t_ECG

        #CREAZIONE FILTRO NOTCH + #STAMPA  GRAFICO della risposta in frequanza del filtro notch colore=verde
        b_ECG,a_ECG = Wemos_MB.notch(fc_ECG/2.0)

        #APPLICAZIONE FILTRO NOTCH A ECG
        filtered_ECG = Wemos_MB.ff(b_ECG, a_ECG, ECG_data)

        #CREAZIONE PASSABASSO PER GSR + #STAMPA FILTRO PASSABASSO per GSR colore=verde
        lw_p = Wemos_MB.low_pass(fc_GSR, fc_ECG/2.0)
        
        #APPLICAZIONE FILTRO al GSR 
        filtered_GSR = Wemos_MB.ff(lw_p, 1.0, GSR_data)

        #GRAFICO FILTRATO ECG
        #plt.title('ECG filtered: ')
        #plt.plot(t_ECG, filtered_ECG/ADC, 'r')
        #plt.grid(True)
        #plt.show()


        #GRAFICO FILTRATO GSR
        #plt.figure(4)
        #plt.title('GSR filtered: ')
        #plt.plot(t_GSR, filtered_GSR/ADC, 'r')
        #plt.grid(True)
        #plt.show()

        #CONVOLUZIONE
        CONV = np.convolve([1/12,-2/3,0,2/3,-1/12], filtered_GSR, 'same') #introduce ritardo pari al numero dei coeff -1-->4
        conv_GSR=(CONV-np.mean(CONV))/np.std(CONV)

        #STAMPA GRAFICO GSR APPLICAZIONE della CONVOLUZIONE(ROSSO)
        #plt.figure()
        #plt.title('GSR CONVOLUTION: ')
        #plt.plot(t_GSR, conv_GSR, 'r') #diviso per scarto quadratico medio
        #plt.axis([0, max(t_GSR), -2, 2])
        #plt.xlabel('Time [s]')
        #plt.ylabel('Skin conductance [\u03bcS]')
        #plt.grid(True)
        #plt.show()

        #ALGORITMO DI PAN TOMPKINS
        t_peak = Wemos_MB.pan_tompkins(5, 15, fc_ECG/2.0, ECG_data, t_ECG, soglia)
        print("Time peak RR: " + str(t_peak))

        file_name = ID + ".pickle"

        with open(file_name, "wb") as f:
            pickle.dump((t_ECG,filtered_ECG/ADC,conv_GSR,t_peak), f)

        print("creato documento pickle")



        i = 0
        iw1 = 30
        iw2 = 90
        while i <= t_ECG[-1]:
           plt.title('Window ECG - neutral')
           plt.plot(t_ECG[np.where(np.logical_and(t_ECG>i,t_ECG<i+iw1))], filtered_ECG[np.where(np.logical_and(t_ECG>i,t_ECG<i+iw1))]/ADC, 'r') 
           plt.xlabel('Time [s]')
           plt.ylabel('Voltage [V]')
           plt.grid(True)
           plt.show()
           plt.title('Window ECG - emotive')
           plt.plot(t_ECG[np.where(np.logical_and(t_ECG>i+iw1,t_ECG<i+iw1+iw2))], filtered_ECG[np.where(np.logical_and(t_ECG>i+iw1,t_ECG<i+iw1+iw2))]/ADC, 'r') 
           plt.xlabel('Time [s]')
           plt.ylabel('Voltage [V]')
           plt.grid(True)
           plt.show()
           i = i+iw1+iw2
        
        #print("Position: "+ str(m))
        #print("-->Peak: " + str(peak[:k]))
        #print("-->Tpeak: " + str(tpeak[:k]))
        #MEDIE GSR
        GSR_mean_NEUTRAL, GSR_mean_EMOTION = Wemos_MB.window(conv_GSR,t_GSR)
        #print("GSR average = "+ str(GSR_mean))
        #for i in range(0,4):
        #    print("GSR avarage related to the", i, "window of the NEUTRAL image " + str(GSR_mean_NEUTRAL[i]))
        #    print("GSR avarage related to the", i, "window of the EMOTIVE images " + str(GSR_mean_EMOTION[i]))

        #CONTROLLO TEMPERATURA INIZIALE-FINALE
        print("TMP data= ", tmp_data)
        deltaTMP = Wemos_MB.tmp(tmp_data)
        print("deltaTMP = ", deltaTMP)
        if deltaTMP>3:
            print("High temperature variation during the test--> REPEAT the test!")
  
        #var = np.mean(np.abs(conv_GSR - GSR_mean)**2)
        #print("GSR variance1 = "+str(var))
        #GSR_var = np.var(conv_GSR, out=None)
        #print("GSR variance2 = "+ str(GSR_var))
        #GSR_std = np.std(conv_GSR)
        #print("GSR standard deviation = "+ str(GSR_std))

        #STAMPA TACOGRAMMA x=numero battiti, y=tempo tra un battito ed il successivo
        x = np.arange(len(t_peak)-1)
        y = np.diff(t_peak, 1, axis=0)
        plt.title('TACHOGRAM: ')
        plt.plot(x, y, 'r') 
        plt.xlabel('Beat #')
        plt.ylabel('R-R [s]')
        plt.grid(True)
        plt.show()

        i = 0
        m=0
        iw1 = 30
        iw2 = 90
        t_peak_std_neutral=np.zeros(2)
        t_peak_var_neutral=np.zeros(2)
        t_peak_rms_neutral=np.zeros(2)

        t_peak_std_emotive=np.zeros(2)
        t_peak_var_emotive=np.zeros(2)
        t_peak_rms_emotive=np.zeros(2)

        while i <= t_peak[-1]:
            t_peak_W1 = t_peak[np.where(np.logical_and(t_peak>i,t_peak<i+iw1))]
            y1 = np.diff(t_peak_W1, 1, axis=0)
            
            t_peak_std_neutral[m] = np.std(y1)
            t_peak_var_neutral[m] = np.var(y1, out=None)
            t_peak_rms_neutral[m] = np.sqrt(np.mean(np.square(y1)))

            t_peak_W2 = t_peak[np.where(np.logical_and(t_peak>i+iw1,t_peak<i+iw1+iw2))]
            y2 = np.diff(t_peak_W2, 1, axis=0)
            t_peak_std_emotive[m] = np.std(y2)
            t_peak_var_emotive[m] = np.var(y2, out=None)
            t_peak_rms_emotive[m] = np.sqrt(np.mean(np.square(y2)))

            i = i+iw1+iw2
            m = m+1

        print('Standard deviation NEUTRAL: ',t_peak_std_neutral)
        print('Standard deviation EMOTIVE: ',t_peak_std_emotive)
        print('Variance NEUTRAL: ',t_peak_var_neutral)
        print('Variance EMOTIVE: ',t_peak_var_emotive)
        print('Root mean square NEUTRAL: ',t_peak_rms_neutral)
        print('Root mean square EMOTIVE: ',t_peak_rms_emotive)
        #iw = 20
        #for i in range(0,len()):
        #   plt.figure(4+i)
        #   plt.title('window TACHOGRAM: ')
        #   plt.plot(x[i*iw:(i+1)*iw], y[i*iw:(i+1)*iw], 'r') 
        #   plt.xlabel('Beat #')
        #   plt.ylabel('R-R [s]')
        #   plt.grid(True)
        #   plt.show()

        #analisi statistiche
        #t_peak_std = np.std(y)
        #print('Standard deviation: ', t_peak_std)
        #t_peak_var = np.var(y, out=None)
        #print('Variance: ', t_peak_var)
        #t_peak_rms = np.sqrt(np.mean(np.square(y)))
        #print('Root mean square: ', t_peak_rms)

        #analyze_ecg(t_peak)

        #CALCOLO LF e HF
        

        dat = "\nStandard deviation NEUTRAL: "+str(t_peak_std_neutral)+"\nStandard deviation EMOTIVE: "+ str(t_peak_std_emotive)+"\nVariance NEUTRAL: "+str(t_peak_var_neutral)+"\nVariance EMOTIVE: "+str(t_peak_var_emotive)+"\nRoot mean square NEUTRAL: "+str(t_peak_rms_neutral)+"\nRoot mean square EMOTIVE: "+str(t_peak_rms_emotive)+"\nGSR mean Neutral: "+str(GSR_mean_NEUTRAL)+"\nGSR mean Emotive: "+str(GSR_mean_EMOTION)
        User_data = open("Users_data.txt", "a")
        User_data.write(dat)
        User_data.close

        LFHF = Wemos_MB.HRV(t_peak)

        User_data = open("Users_data.txt", "a")
        User_data.write(LFHF)
        User_data.close
       